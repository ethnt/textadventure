# TextAdventure

A very basic text adventure.

## Story

- **Theme and backstory**: You're alone on a college campus. What happened? Who are you? When is it?
- **World and locations**: The Marist College campus. Donnelly and Hancock are the only accessible locations, and all other buildings are locked.
- **Items and objects**: Nothing but your ID card and an empty wallet.
- **Goals and obstacles**: Why is the campus empty? Where are all the people?
