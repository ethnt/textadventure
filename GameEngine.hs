-- GameEngine.hs

gameIntro :: IO ()
gameIntro = do
  putStrLn "Welcome to TEXT ADVENTURE"
  putStrLn "It's quite boring, really."
  putStrLn "I mean, there's really no point going on from here, there's not much for you to do."
  putStrLn ""

gameCredits :: IO ()
gameCredits = do
  putStrLn "Code and Story: Ethan Turkeltaub"
  putStrLn ""
  putStrLn ""

gameLoop :: Int -> IO ()
gameLoop moves = do
  putStrLn "What do you want to do?"

  command <- getLine

  putStrLn ""

  if command == "quit"
    then return ()
    else do
      putStrLn ("You have progressed " ++ show moves ++ " moves.")
      putStrLn ""
      putStrLn ""
      putStrLn ""
      putStrLn ""
      gameLoop (moves + 1)

main :: IO ()
main = do
  gameIntro
  gameCredits

  gameLoop 0
